CREATE TABLE IF NOT EXISTS public.bugdet
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 ),
    version bigint NOT NULL DEFAULT 1,
    account_ids bigint[] NOT NULL,
    amount numeric(19, 2) NOT NULL DEFAULT 0,
    category_ids bigint[] NOT NULL,
    name character varying NOT NULL,
    period integer NOT NULL,
    created_at date NOT NULL,
    updated_at date,
    PRIMARY KEY (id)
);

ALTER TABLE public.bugdet
    OWNER to postgres;